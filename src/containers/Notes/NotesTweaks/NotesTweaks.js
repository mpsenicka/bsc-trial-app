import React, { Component } from 'react';

import { createNote, getNote, updateNote } from '../../../RestApi/RestApi';
import './NotesTweaks.css';
import { FormattedMessage } from "react-intl";

class NotesTweaks extends Component {
  state = {
    title: ''
  };

  componentDidMount() {
    if (this.type === 'edit') {
      getNote(this.props.match.params.id).then(res => {
        this.setState({
          title: res.data.title
        });
      });
    }
  }

  /* EDIT TITLE HANDLER */
  editTitleHandler = tit => {
    this.setState({
      title: tit.currentTarget.value
    });
  };

  /* NEW TITLE HANDLER */
  titleHandler = () => {
    switch (this.type) {
      case 'new':
        createNote(this.state.title).then(() => this.props.history.push('/'));
        console.log('created');
        break;

      case 'edit':
        updateNote(this.props.match.params.id, this.state.title).then(() => {
          this.props.history.push('/');
          console.log('updated');
        });
        break;

      default:
        prompt('Wrong type');
    }
  };

  render() {
    const id = this.props.match.params.id;
    this.type = id ? 'edit' : 'new';

    return (
      <div>
        <div className='form-group form'>
          <textarea
            className='form-control'
            rows='7'
            value={this.state.title}
            onChange={this.editTitleHandler}
          />
          <button className='submit-button' onClick={() => this.titleHandler()}>
          <FormattedMessage id="submit" defaultMessage="Submit" />
          </button>
        </div>
        <p className='preview'>
          <strong><FormattedMessage id="note_preview" defaultMessage="Note Preview:" /></strong> <br />
          {this.state.title}
        </p>
      </div>
    );
  }
}

export default NotesTweaks;
