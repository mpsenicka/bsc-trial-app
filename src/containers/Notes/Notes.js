import React, { Component } from 'react';
import { getNotes, deleteNote } from '../../RestApi/RestApi';

import OneNote from '../Notes/OneNote/OneNote';
import {FormattedMessage} from 'react-intl';

class Notes extends Component {
  state = {
    data: null
  };

  getData() {
    getNotes().then(response => {
      this.setState({
        data: response.data
      });
    });
  }

  componentDidMount() {
    this.getData();
  }

  /* RENDER ALL THE NOTES */
  renderNotesHandler = () => {
    if (this.state.data === null) {
      return (
        <tr>
          <td colSpan='3'>Loading data</td>
        </tr>
      );
    } else if (this.state.data.length === 0) {
      return (
        <tr>
          <td colSpan='3'>No data</td>
        </tr>
      );
    } else {
      return (
        <OneNote
          delete={this.deleteNoteHandler}
          notes={this.state.data}
        />
      );
    }
  };

  /* DELETE NOTE HANDLER */
  deleteNoteHandler = id => {
    deleteNote(id).then(res => {
      console.log('deleted');
      this.getData();
    });
  };


  render() {
    return (
      <table className='table table-striped'>
        <thead>
          <tr>
            <th scope='col'>ID</th>
            <th scope='col'><FormattedMessage id="title" defaultMessage="Title" /></th>
            <th scope='col'><FormattedMessage id="action" defaultMessage="Action" /></th>
          </tr>
        </thead>
        <tbody>{this.renderNotesHandler()}</tbody>
      </table>
    );
  }
}

export default Notes;
