import React from 'react';

import { Link } from 'react-router-dom';
import './OneNote.css';
import { FormattedMessage } from "react-intl";

const note = props => {
  return props.notes.map(nt => {
    return (
      <tr key={nt.id}>
        <td>{nt.id}</td>
        <td>{nt.title}</td>
        <td>
            <div className='delEd link' onClick={() => props.delete(nt.id)}><FormattedMessage id='delete' defaultMessage='Delete'/></div>
            <Link className='delEd link' to={'/edit/' + nt.id}><FormattedMessage id='edit' defaultMessage='Edit'/></Link>
        </td>
      </tr>
    );
  });
};

export default note;
