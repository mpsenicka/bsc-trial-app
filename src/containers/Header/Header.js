import React from 'react';
import { Link } from 'react-router-dom';
import './Header.css';
import { FormattedMessage } from 'react-intl';

const Menu = ({ changeLanguageHandler }) => {
  return (
    <nav className='navigation'>
      <Link className='link' to='/'>
        <div className='nav-el'>
          <FormattedMessage id='home' defaultMessage='Home' />
        </div>
      </Link>
      <Link className='link' to='/new'>
        <div className='nav-el'>
          <FormattedMessage id='new_note' defaultMessage='New Note' />
        </div>
      </Link>
      <div className='nav-el link' onClick={() => changeLanguageHandler('cs')}>
        Czech
      </div>
      <div className='nav-el link' onClick={() => changeLanguageHandler('en')}>
        English
      </div>
    </nav>
  );
};

export default Menu;
